import tree.BinaryTreePainter;
import util.SwingUtils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;

public class DemoPage extends JFrame{
	private JPanel panelMain;
	private JTextField textFieldBracketNotationTree;
	private JButton buttonMakeTree;
	private JPanel panelPaintArea;
	private JButton buttonSaveImage;
	private JCheckBox checkBoxTransparent;
	private JButton reformatTree;

	private JMenuBar menuBarMain;
	private JPanel paintPanel = null;
	private JFileChooser fileChooserSave;


	SimpleBinaryTree tree = new SimpleBinaryTree();


	public DemoPage() {
		this.setTitle("Двоичные деревья");
		this.setContentPane(panelMain);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();

		paintPanel = new JPanel() {
			private Dimension paintSize = new Dimension(0, 0);

			@Override
			public void paintComponent(Graphics gr) {
				super.paintComponent(gr);
				paintSize = BinaryTreePainter.paint(tree, gr);
				if (!paintSize.equals(this.getPreferredSize())) {
					SwingUtils.setFixedSize(this, paintSize.width, paintSize.height);
				}
			}
		};
		JScrollPane paintJScrollPane = new JScrollPane(paintPanel);
		panelPaintArea.add(paintJScrollPane);

		fileChooserSave = new JFileChooser();
		fileChooserSave.setCurrentDirectory(new File("./images"));
		FileFilter filter = new FileNameExtensionFilter("SVG images", "svg");
		fileChooserSave.addChoosableFileFilter(filter);
		fileChooserSave.setAcceptAllFileFilterUsed(false);
		fileChooserSave.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooserSave.setApproveButtonText("Save");

		buttonMakeTree.addActionListener(actionEvent -> {
			try {
				SimpleBinaryTree tree = new SimpleBinaryTree(Integer::parseInt);
				tree.fromBracketNotation(textFieldBracketNotationTree.getText());
				this.tree = tree;
				repaintTree();
			} catch (Exception ex) {
				SwingUtils.showErrorMessageBox(ex);
			}
		});


		buttonSaveImage.addActionListener(actionEvent -> {
			if (tree == null) {
				return;
			}
			try {
				if (fileChooserSave.showSaveDialog(DemoPage.this) == JFileChooser.APPROVE_OPTION) {
					String filename = fileChooserSave.getSelectedFile().getPath();
					if (!filename.toLowerCase().endsWith(".svg")) {
						filename += ".svg";
					}
					BinaryTreePainter.saveIntoFile(tree, filename, checkBoxTransparent.isSelected());
				}
			} catch (Exception e) {
				SwingUtils.showErrorMessageBox(e);
			}
		});

		reformatTree.addActionListener(e -> {
			try {
				tree.addExtraLeaves();
				repaintTree();
			} catch (Exception ex) {
				SwingUtils.showErrorMessageBox(ex);
			}
		});

	}


	/**
	 * Перерисовка дерева
	 */
	public void repaintTree() {
		//panelPaintArea.repaint();
		paintPanel.repaint();
		//panelPaintArea.revalidate();
	}
}
